﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using RestaurantPlatformMobileApp.Entities;

namespace RestaurantPlatformMobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderView : ContentPage
    {
        private OrderListByUserVM _selectOrderItem;
        public OrderView(OrderListByUserVM selectOrderItem)
        {
            InitializeComponent();
            this._selectOrderItem = selectOrderItem;
            BindingContext = this._selectOrderItem;
            this.Appearing += CP_Appearing;
        }

        private void CP_Appearing(object sender, EventArgs e)
        {
            this.GetOrderItemList();
        }

        private void GetOrderItemList()
        {
            this.OrderItemList.ItemsSource = null;
            this.OrderItemList.ItemsSource = this._selectOrderItem.OrderItem;
        }

        async void OnCloseOrderViewClicked(object sender, EventArgs args)
        {
            // Page appearance not animated
            await Navigation.PopModalAsync(false);
        }

        private float GetTotalOrderItem()
        {
            float total = 0;
            foreach(OrderItem item in this._selectOrderItem.OrderItem)
            {
                total += item.Price * item.Quantity;

            }
            return total;
        }

    }
}