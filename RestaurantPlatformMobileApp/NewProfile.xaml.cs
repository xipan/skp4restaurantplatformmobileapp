﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Net.Http;
using System.Text.Json;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using RestaurantPlatformMobileApp.Entities;

namespace RestaurantPlatformMobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewProfile : ContentPage
    {
        private bool mailAdresseValidation = false;
        private bool passwordValidation = false;
        private bool firstNameValidation = false;
        private bool afterNameValidation = false;

        public NewProfile()
        {
            InitializeComponent();
        }

        async void OnDismissButtonClicked(object sender, EventArgs args)
        {
            // Page appearance not animated
            await Navigation.PopModalAsync(false);
        }


        private async void OnCreateProfileClick(object sender, EventArgs e)
        {
            this.CreateProfileButton.IsEnabled = false;

            User newItem = new User{ 
                UserID = 0, 
                FirstName = this.FirstName.Text.ToString(), 
                AfterName = this.AfterName.Text.ToString(), 
                Email = this.MailAdresse.Text.ToString(), 
                PassWord = this.Password.Text.ToString(), 
                UserType = 1 
            };


            HttpClient client = new HttpClient();
            
            var uri = new Uri(string.Format("http://10.0.6.13/api/user/"));
            var json = JsonSerializer.Serialize(newItem);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = null;
            response = await client.PostAsync(uri, content);

            if (response.IsSuccessStatusCode)
            {
                await DisplayAlert("Info", "Velkommen. Log ind med din e-mailadresse", "OK");
                await Navigation.PopModalAsync(false);
            }
            else
            {
                this.CreateProfileButton.IsEnabled = true;
            }


        }

        private void MailAdresseValidation(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            String val = entry.Text; 

            string expression = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

            if (Regex.IsMatch(val, expression) || (val.Length == 0) )
            {
                this.MailAdresseValidationText.Text = "";
                this.mailAdresseValidation = true;
            }
            else
            {
                this.MailAdresseValidationText.Text = "Ugyldig e-mail adresse";
                this.mailAdresseValidation = false;
            }
            this.SetCreateProfilButtonEnable();
        }


        private void PasswordValidation(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            String val = entry.Text;

            if ((val.Length >=5) && (val.Length <=30))
            {
                this.PasswordValidationText.Text = "";
                this.passwordValidation = true;
            }
            else
            {
                this.PasswordValidationText.Text = "Adgangskoden skal være på mellem 5 og 30 tegn.";
                this.passwordValidation = false;
            }
            this.SetCreateProfilButtonEnable();

        }

        private void FirstNameValidation(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            String val = entry.Text;

            if ((val.Length >= 2) && (val.Length <= 25))
            {
                this.FirstNameValidationText.Text = "";
                this.firstNameValidation = true;
            }
            else
            {
                this.FirstNameValidationText.Text = "Fornavn skal være mellem 2 og 25 tegn langt.";
                this.firstNameValidation = false;
            }
            this.SetCreateProfilButtonEnable();

        }

        private void AfterNameValidation(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            String val = entry.Text;

            if ((val.Length >= 2) && (val.Length <= 25))
            {
                this.AfterNameValidationText.Text = "";
                this.afterNameValidation = true;
            }
            else
            {
                this.AfterNameValidationText.Text = "Efternavn skal være mellem 2 og 25 tegn langt.";
                this.afterNameValidation = false;
            }
            this.SetCreateProfilButtonEnable();

        }

        private void SetCreateProfilButtonEnable()
        {

            if ( (this.mailAdresseValidation) && (this.passwordValidation) && (this.firstNameValidation) && (this.afterNameValidation) )
            {
                this.CreateProfileButton.IsEnabled = true;
            }
            else
            {
                this.CreateProfileButton.IsEnabled = false;
            }
        }

    }


}