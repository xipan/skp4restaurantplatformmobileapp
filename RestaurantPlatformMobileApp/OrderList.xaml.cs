﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text.Json;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using RestaurantPlatformMobileApp.Entities;

namespace RestaurantPlatformMobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderList : ContentPage
    {
        private CurrentUserSingleton currentUser = CurrentUserSingleton.Instance;

        public OrderList()
        {
            InitializeComponent();
            this.Appearing += CP_Appearing;


            OrderListLV.ItemSelected += (sender, e) => {
                if (e.SelectedItem == null) return;
                Navigation.PushModalAsync(new OrderView(e.SelectedItem as OrderListByUserVM));
            };

        }

        async void OnCloseOrderListClicked(object sender, EventArgs args)
        {
            // Page appearance not animated
            await Navigation.PopModalAsync(false);
        }

        private void CP_Appearing(object sender, EventArgs e)
        {
            this.GetOrderList();
        }

        public async void GetOrderList()
        {
            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format(currentUser.ApiServer.ToString() + "api/order/" + currentUser.CurrentUser.UserID.ToString()));
            HttpResponseMessage response = null;
            response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var returnContent = await response.Content.ReadAsStringAsync();
                List<OrderListByUserVM> items = JsonSerializer.Deserialize<List<OrderListByUserVM>>(returnContent);

                this.OrderListLV.ItemsSource = null;
                this.OrderListLV.ItemsSource = items;
            }
            else
            {
            }

        }
    }
}