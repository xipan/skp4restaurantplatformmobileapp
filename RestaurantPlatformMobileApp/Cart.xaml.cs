﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using RestaurantPlatformMobileApp.Entities;
using System.Net.Http;
using System.Text.Json;

namespace RestaurantPlatformMobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Cart : ContentPage
    {
        private CurrentUserSingleton currentUser = CurrentUserSingleton.Instance;

        public Cart()
        {
            InitializeComponent();
            BindingContext = currentUser;

            this.Appearing += CP_Appearing;

            CartList.ItemSelected += async (sender, e) => {
                if (e.SelectedItem == null) return;
                //e.SelectedItem as RestaurantPlatformMobileApp.Entities.Cart
                //await PopupNavigation.Instance.PushAsync(new AddCart(e.SelectedItem as RestaurantPlatformMobileApp.Entities.MenuItem));
            };

        }

        private void CP_Appearing(object sender, EventArgs e)
        {
            this.CartList.ItemsSource = null;
            this.CartList.ItemsSource = currentUser.Cart.CartItem;

            if ( currentUser.CurrentSelectedRestaurant != null )
            {
                this.RestaurantName.Text = currentUser.CurrentSelectedRestaurant.RestaturantName.ToString();
            }
            
        }

        private async void DelCartItem(object sender, EventArgs e)
        {
            var args = (TappedEventArgs)e;
            CartItem selectedItem = currentUser.Cart.CartItem.Find(cm => cm.CartItemID == int.Parse(args.Parameter.ToString()));
            if (selectedItem != null)
            {
                currentUser.Cart.CartItem.Remove(selectedItem);
                currentUser.UpdateCartProperty();
                this.CartList.ItemsSource = null;
                this.CartList.ItemsSource = currentUser.Cart.CartItem;
            }
            await DisplayAlert("Info", "Varerne blive fjernet", "OK");
        }

        private async void CheckOut(object sender, EventArgs e)
        {
            if (currentUser.CartCount != string.Empty)
            {
                HttpClient client = new HttpClient();

                var uri = new Uri(string.Format(currentUser.ApiServer.ToString() + "api/order/" + currentUser.Cart.CartID.ToString()));
                HttpResponseMessage response = null;
                response = await client.PostAsync(uri, null);

                if (response.IsSuccessStatusCode)
                {
                    var returnContent = await response.Content.ReadAsStringAsync();
                    if (returnContent.Length > 0)
                    {
                        currentUser.Cart = new Entities.Cart();
                        currentUser.InitCart();                        
                        this.CartList.ItemsSource = null;
                        this.CartList.ItemsSource = currentUser.Cart.CartItem;
                        await DisplayAlert("Info", "Din ordre blive afleveret", "OK");
                    }
                }
            }
            else
            {
                await DisplayAlert("Info", "Cart er tomt", "OK");
            }
        }


    }
}