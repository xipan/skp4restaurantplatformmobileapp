﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using RestaurantPlatformMobileApp.Entities;

using System.ComponentModel;

namespace RestaurantPlatformMobileApp
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NavigationPage : TabbedPage
    {
        private CurrentUserSingleton currentUser = CurrentUserSingleton.Instance;

        public NavigationPage()
        {
            InitializeComponent();
            BindingContext = currentUser;

            //this.Cart.Title = "kr. 2.234";

            this.CurrentPageChanged += (object sender, EventArgs e) => {
            
                var i = this.Children.IndexOf(this.CurrentPage);

                switch (i)
                {
                    case 0: //MenuList 
                        this.MenuList.IconImageSource = "ic_action_restaurant.png";
                        this.Service.IconImageSource = "ic_action_notifications_active_dark.png";
                        this.Profile.IconImageSource = "ic_action_supervisor_account_dark.png";
                        this.Cart.IconImageSource = "ic_action_shopping_cart_dark.png";
                        break;
                    case 1: //Service
                        this.MenuList.IconImageSource = "ic_action_restaurant_dark.png";
                        this.Service.IconImageSource = "ic_action_notifications_active.png";
                        this.Profile.IconImageSource = "ic_action_supervisor_account_dark.png";
                        this.Cart.IconImageSource = "ic_action_shopping_cart_dark.png";
                        break;
                    case 2: //Profile
                        this.MenuList.IconImageSource = "ic_action_restaurant_dark.png";
                        this.Service.IconImageSource = "ic_action_notifications_active_dark.png";
                        this.Profile.IconImageSource = "ic_action_supervisor_account.png";
                        this.Cart.IconImageSource = "ic_action_shopping_cart_dark.png";
                        break;
                    case 3: //Cart
                        this.MenuList.IconImageSource = "ic_action_restaurant_dark.png";
                        this.Service.IconImageSource = "ic_action_notifications_active_dark.png";
                        this.Profile.IconImageSource = "ic_action_supervisor_account_dark.png";
                        this.Cart.IconImageSource = "ic_action_shopping_cart.png";
                        break;
                }

            };
            //this.ToolbarItems
        }
        
    }

}