﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using RestaurantPlatformMobileApp.Entities;
using Rg.Plugins.Popup.Services;


namespace RestaurantPlatformMobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuList : ContentPage
    {
        private CurrentUserSingleton currentUser = CurrentUserSingleton.Instance;

        public MenuList()
        {
           
            InitializeComponent();
            CreateRestaurantList();            

            
            RestaurList.ItemSelected += (sender, e) => {
                if (e.SelectedItem == null) return;
                this.RestaurListWrap.IsVisible = false;
                this.CategoryListWrap.IsVisible = true;
                this.RestaurantMenuListWrap.IsVisible = false;
                CreateMenuListOfCatergory(e.SelectedItem as Restaurant);

                this.BreadCrumbsOfRestaurant.Text = " " + (e.SelectedItem as Restaurant).RestaturantName;

                this.currentUser.CurrentSelectedRestaurant = e.SelectedItem as Restaurant;
                this.currentUser.InitCart();

            };

            CategoryList.ItemSelected +=  (sender, e) => {
                if (e.SelectedItem == null) return;
                this.RestaurListWrap.IsVisible = false;
                this.CategoryListWrap.IsVisible = false;
                this.RestaurantMenuListWrap.IsVisible = true;
                CreateMenuItem(e.SelectedItem as RestaurantPlatformMobileApp.Entities.Menu);

                this.BreadCrumbsOfCategory.Text = " > " + (e.SelectedItem as RestaurantPlatformMobileApp.Entities.Menu).CategoryName;
            };

            RestaurantMenuList.ItemSelected += async (sender, e) => {
                if (e.SelectedItem == null) return;
                //e.SelectedItem as RestaurantPlatformMobileApp.Entities.Cart
                await PopupNavigation.Instance.PushAsync(new AddCart(e.SelectedItem as RestaurantPlatformMobileApp.Entities.MenuItem));
            };
        }



        async void OnDismissButtonClicked(object sender, EventArgs args)
        {
            // Page appearance not animated
            await DisplayAlert("Info", "Velkommen.", "OK");
        }

        public void OnRestaurListWrapButtonClicked(object sender, EventArgs args)
        {
            this.RestaurListWrap.IsVisible = true;
            this.CategoryListWrap.IsVisible = false;
            this.RestaurantMenuListWrap.IsVisible = false;

            this.BreadCrumbsOfRestaurant.Text = "";
            this.BreadCrumbsOfCategory.Text = "";


        }

        public void OnCategoryListWrapButtonClicked(object sender, EventArgs args)
        {
            this.RestaurListWrap.IsVisible = false;
            this.CategoryListWrap.IsVisible = true;
            this.RestaurantMenuListWrap.IsVisible = false;

            this.BreadCrumbsOfCategory.Text = "";
        }


        public async void CreateRestaurantList()
        {
            HttpClient client = new HttpClient();
            var currentUser = CurrentUserSingleton.Instance;

            var uri = new Uri(string.Format(currentUser.ApiServer.ToString() + "api/restaurant"));
            HttpResponseMessage response = null;
            response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var returnContent = await response.Content.ReadAsStringAsync();
                List<Restaurant> items = JsonSerializer.Deserialize<List<Restaurant>>(returnContent);
                this.RestaurList.ItemsSource = items;
                //await DisplayAlert("Info", "Velkommen.", "OK");

            }
            else
            {

            }

        }

        public async void CreateMenuListOfCatergory(Restaurant selectedItem)
        {
            HttpClient client = new HttpClient();
            var currentUser = CurrentUserSingleton.Instance;

            var uri = new Uri(string.Format(currentUser.ApiServer.ToString() + "api/menu/" + selectedItem.RestaurantID.ToString()));
            HttpResponseMessage response = null;
            response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var returnContent = await response.Content.ReadAsStringAsync();
                List<RestaurantPlatformMobileApp.Entities.Menu> items = JsonSerializer.Deserialize<List<RestaurantPlatformMobileApp.Entities.Menu>>(returnContent);
                this.CategoryList.ItemsSource = items;
                this.RestaurListWrap.IsVisible = false;
                this.CategoryListWrap.IsVisible = true;
                this.RestaurantMenuListWrap.IsVisible = false;
            }
            else
            {

            }

        }

        public void CreateMenuItem(RestaurantPlatformMobileApp.Entities.Menu selectedItem)
        {
            this.RestaurantMenuList.ItemsSource = selectedItem.MenuItem;
        }

    }
}