﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text.Json;
using RestaurantPlatformMobileApp.Entities;
using Xamarin.Forms;


namespace RestaurantPlatformMobileApp
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private bool mailAdresseValidation = false;
        private bool passwordValidation = false;
        private CurrentUserSingleton currentUser = CurrentUserSingleton.Instance;


        public MainPage()
        {
            InitializeComponent();
        }

        private void MailAdresseValidation(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            String val = entry.Text;

            string expression = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

            if (Regex.IsMatch(val, expression) || (val.Length == 0))
            {
                this.MailAdresseValidationText.Text = "";
                this.mailAdresseValidation = true;
            }
            else
            {
                this.MailAdresseValidationText.Text = "Ugyldig e-mail adresse";
                this.mailAdresseValidation = false;
            }
            this.SetLoginButtonEnable();

        }


        private void PasswordValidation(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            String val = entry.Text;

            if ((val.Length >= 5) && (val.Length <= 30))
            {
                this.PasswordValidationText.Text = "";
                this.passwordValidation = true;
            }
            else
            {
                this.PasswordValidationText.Text = "Adgangskoden skal være på mellem 5 og 30 tegn.";
                this.passwordValidation = false;
            }
            this.SetLoginButtonEnable();

        }

        private void SetLoginButtonEnable()
        {

            if ((this.mailAdresseValidation) && (this.passwordValidation))
            {
                this.LoginButton.IsEnabled = true;
            }
            else
            {
                this.LoginButton.IsEnabled = false;
            }
        }

        private async void OnLoginButtonClick(object sender, EventArgs e)
        {
            //await Navigation.PushModalAsync(new RestaurantList());
            //await Navigation.PushAsync(new RestaurantList());            
            User newItem = new User
            {
                Email = this.MailAdresse.Text.ToString(), //"xipan@gmail.com", 
                PassWord = this.Password.Text.ToString()//"1234567" 
            };


            HttpClient client = new HttpClient();



            //var uri = new Uri(string.Format(this.currentUser.ApiServer.ToString() + "api/user/login"));
            bool isLoginAsAdmin = this.IsLoginAsAdmin.IsToggled;
            var uri = new Uri(string.Format(this.currentUser.ApiServer.ToString() + (isLoginAsAdmin ? "api/Employee/Login" : "api/user/login")));

            var json = JsonSerializer.Serialize(newItem);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = null;
            response = await client.PostAsync(uri, content);

            if ( response.ReasonPhrase == "OK" )
            {
                var returnContent = await response.Content.ReadAsStringAsync();
                var options = new JsonSerializerOptions
                {
                    AllowTrailingCommas = true
                };
                User item = JsonSerializer.Deserialize<User>(returnContent, options);
                if (item.UserID > 0)
                {
                    this.currentUser.CurrentUser = item;
                    //currentuser.CurrentUser = item;
                    //App.Current.MainPage = new RestaurantList();
                    if ( isLoginAsAdmin )
                    {
                        App.Current.MainPage = new EmployeeNavigationPage();
                    }
                    else
                    {
                        App.Current.MainPage = new NavigationPage();
                    }
                    
                }                
            }
            else
            {
                await DisplayAlert("Info", "Der opstod et problem med at logge ind.", "OK");
            }

        }

        private async void OnNewProfileButtonClick(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NewProfile());
        }

    }

}
