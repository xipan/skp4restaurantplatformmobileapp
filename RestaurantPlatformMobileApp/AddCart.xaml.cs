﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using RestaurantPlatformMobileApp.Entities;

namespace RestaurantPlatformMobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddCart : Rg.Plugins.Popup.Pages.PopupPage
    {
        private RestaurantPlatformMobileApp.Entities.MenuItem currentSelectedItem;

        private CurrentUserSingleton currentUser = CurrentUserSingleton.Instance;


        public AddCart(RestaurantPlatformMobileApp.Entities.MenuItem selectedItem)
        {       
            InitializeComponent();
            this.MenuItemName.Text = selectedItem.MenuItemName.ToString();
            this.Price.Text = selectedItem.PriceWithFormat.ToString();
            this.currentSelectedItem = selectedItem;
        }

        public void OnAddItemToCartButtonClicked(object sender, EventArgs args)
        {
            this.currentUser.AddCart(
                            new Entities.CartItem
                            {
                                MenuItemName = this.currentSelectedItem.MenuItemName,
                                Price = this.currentSelectedItem.Price,
                                Quantity = Convert.ToInt32(this.stepper.Value),
                                Status = 0,
                                RefMenuItemID = this.currentSelectedItem.MenuItemID,
                            }
                );
            //this.currentUser.CartCount = "3";
        }

    }
}