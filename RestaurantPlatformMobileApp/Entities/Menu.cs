﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantPlatformMobileApp.Entities
{
    public class Menu
    {
        public int MenuCategoryID { get; set; }
        public string CategoryName { get; set; }

        public int RestaurantID { get; set; }

        public List<MenuItem> MenuItem { get; set; }

        public string MenuDetail { 
            get 
            {
                string resultString = "";
                foreach (var item in MenuItem)
                {
                    resultString += item.MenuItemName + "  ";
                }
                return resultString;
            } 
        }
    }

    public class MenuItem
    {
        public int MenuItemID { get; set; }
        public string MenuItemName { get; set; }
        public float Price { get; set; }
        public string Description { get; set; }

        public string PriceWithFormat
        {
            get
            {                
                return Price.ToString("C2");
            }
        }
    }
}
