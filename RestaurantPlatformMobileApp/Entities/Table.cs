﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantPlatformMobileApp.Entities
{
    public class Table
    {
        public int TableID { get; set; }
        public string TableLabel { get; set; }
        public int TableStatus { get; set; }
        public int RestaurantID { get; set; }        
    }
}
