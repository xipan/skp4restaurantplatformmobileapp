﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;


namespace RestaurantPlatformMobileApp.Entities
{
    public class Cart
    {
        public int CartID { get; set; }
        public int RestaurantID { get; set; }
        public int UserID { get; set; }
        public DateTime CreateDT { get; set; }
        public List<CartItem> CartItem { get; set; }
    }

    public class CartItem : INotifyPropertyChanged
    {
        private CurrentUserSingleton currentUser = CurrentUserSingleton.Instance;

        private int quantity;
        public int CartItemID { get; set; }
        public string MenuItemName { get; set; }
        public float Price { get; set; }

        public string PriceWithFormat
        {
            get
            {
                return Price.ToString("C2");
            }
        }

        public string PriceWithQuantityLabel
        {
            get
            {
                var a = 2;
                return PriceWithFormat + " * " + Quantity.ToString();
            }
        }

        public int Quantity {
            get
            {
                return quantity;
            }
            set 
            {
                quantity = value;
                RaisePropertyChanged(nameof(PriceWithQuantityLabel));
                if ( (currentUser.Cart.RestaurantID > 0) && (currentUser.Cart.UserID > 0) )
                {
                    currentUser.UpdateCartProperty();
                }                
            }
        }
        public int Status { get; set; }
        public int RefMenuItemID { get; set; }
        public int CartID { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
