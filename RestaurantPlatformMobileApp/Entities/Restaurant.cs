﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace RestaurantPlatformMobileApp.Entities
{
    public class Restaurant
    {

        public int RestaurantID { get; set; }
        public string RestaturantName { get; set; }
        public string Adresse { get; set; }
        public string Postale { get; set; }
        public string OpenStart { get; set; }
        public string OpenEnd { get; set; }
        public string RestaurantLogo { get; set; }
        public List<Table> Table { get; set; }

        public string OpeningHoursLabel { 
            get 
            {
                
                return string.Format("Åben fra {0} - {1}", 
                    TimeSpan.Parse(OpenStart).ToString(@"hh\:mm"), 
                    TimeSpan.Parse(OpenEnd).ToString(@"hh\:mm")
                    );
            } 
        }

        public string RestaurantLogoFullUrl { 
            get 
            {
                var currentUser = CurrentUserSingleton.Instance;
                var imageSource = currentUser.ResourceUrl + RestaurantLogo;
                return imageSource;
            }
        }

    }
}
