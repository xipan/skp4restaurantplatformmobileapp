﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text.Json;

namespace RestaurantPlatformMobileApp.Entities
{
    class CurrentUserSingleton : INotifyPropertyChanged
    {
        private static CurrentUserSingleton _Instance = null;
        public User CurrentUser;
        public Restaurant CurrentSelectedRestaurant;
        
        public string ApiServer = "http://10.0.6.13/";
        public string ResourceUrl 
        { 
            get
            {
                return ApiServer + "restaurant-logo/";
            }
        }

        public Cart Cart = new Cart();

        public void AddCart(CartItem selectedItem)
        {
            if (this.Cart.CartItem == null)
            {
                this.Cart.CartItem = new List<CartItem>();
            }

            CartItem item = this.Cart.CartItem.FirstOrDefault(mi => mi.RefMenuItemID == selectedItem.RefMenuItemID);
            if (item == null)
            {
                this.Cart.CartItem.Add(selectedItem);
            }
            else
            {
                item.Quantity += selectedItem.Quantity;
            }


            this.UpdateCartProperty();
            //this.UpdateCart();
        }

        public string CartCount {             
            get {
                if (this.Cart.CartItem == null) //this.Cart.Count <= 0
                {
                    return string.Empty;
                }
                else
                {
                    if (this.Cart.CartItem.Count == 0)
                    {
                        return string.Empty;
                    }
                    else
                    {
                        return this.Cart.CartItem.Sum(ci => ci.Quantity).ToString();
                    }
                    
                    
                }                
            } 
        }

        public string TotalOfCart
        {
            get
            {
                float totalPrice = 0;
                if (this.Cart.CartItem != null) //this.Cart.Count <= 0
                {
                    foreach (CartItem item in this.Cart.CartItem)
                    {
                        totalPrice += item.Price * item.Quantity;
                    }
                }
                return totalPrice.ToString("C2");
            }
        }

        public async void InitCart()
        {
            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format(this.ApiServer.ToString() + "api/cart/getcart/" + this.CurrentUser.UserID + "/" + this.CurrentSelectedRestaurant.RestaurantID));

            HttpResponseMessage response = null;
            response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var returnContent = await response.Content.ReadAsStringAsync();
                if (returnContent.Length > 0)
                {
                    RestaurantPlatformMobileApp.Entities.Cart items = JsonSerializer.Deserialize<RestaurantPlatformMobileApp.Entities.Cart>(returnContent);
                    this.Cart = items;                    
                    
                    RaisePropertyChanged(nameof(CartCount));
                    RaisePropertyChanged(nameof(TotalOfCart));
                }
                else
                {
                    this.Cart = new Cart();
                    RaisePropertyChanged(nameof(CartCount));
                    RaisePropertyChanged(nameof(TotalOfCart));
                }
            }
            else
            {

            }
        }

        public async void UpdateCart()
        {
            if (this.Cart.RestaurantID <= 0)
            {
                this.Cart.CreateDT = DateTime.Now;
                this.Cart.RestaurantID = this.CurrentSelectedRestaurant.RestaurantID;
                this.Cart.UserID = this.CurrentUser.UserID;
            }
            Cart newItem = this.Cart;


            HttpClient client = new HttpClient();

            var uri = new Uri(string.Format(this.ApiServer.ToString() + "api/cart"));
            var json = JsonSerializer.Serialize(newItem);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = null;
            response = await client.PostAsync(uri, content);

            if (response.IsSuccessStatusCode)
            {
                var returnContent = await response.Content.ReadAsStringAsync();
                if (returnContent.Length > 0)
                {
                    RestaurantPlatformMobileApp.Entities.Cart items = JsonSerializer.Deserialize<RestaurantPlatformMobileApp.Entities.Cart>(returnContent);
                    this.Cart.CartID = items.CartID;
                }

            }
            else
            {
                
            }

        }
        
        public void UpdateCartProperty()
        {
            RaisePropertyChanged(nameof(CartCount));
            RaisePropertyChanged(nameof(TotalOfCart));
            RaisePropertyChanged(nameof(Cart.CartItem));

            this.UpdateCart();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public static CurrentUserSingleton Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new CurrentUserSingleton();
                }
                return _Instance;
            }
        }

    }
}
