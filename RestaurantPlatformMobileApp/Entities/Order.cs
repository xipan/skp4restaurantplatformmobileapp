﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantPlatformMobileApp.Entities
{
    public class Order
    {
        public int OrderID { get; set; }
        public int UserID { get; set; }
        /// <summary>
        /// 0: Pending, 1: Complete
        /// </summary>
        public int Status { get; set; }
        public DateTime CreateDT { get; set; }

        public int RestaurantID { get; set; }
        public Restaurant Restaurant { get; set; }

        public List<OrderItem> OrderItem { get; set; }
    }

    public class OrderItem
    {
        public int OrderItemID { get; set; }
        public string MenuItemName { get; set; }
        public float Price { get; set; }
        public int Quantity { get; set; }
        /// <summary>
        /// 0: Pending, 1: Canceled, 2: Complete
        /// </summary>
        public int Status { get; set; }
        public int RefMenuItemID { get; set; }
        public int OrderID { get; set; }

        public float TotalOfItem
        {
            get
            {
                return Quantity * Price;
            }
        }


    }
}
