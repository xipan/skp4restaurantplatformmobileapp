﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantPlatformMobileApp.Entities
{
    class TestForUI
    {
        public string CategoryName { get; set; }
        public string ImageUrl { get; set; }

        public List<TestForUI> GetTestForUI()
        {
            List<TestForUI> testforui = new List<TestForUI>()
            {               
                new TestForUI(){CategoryName = "Drikke", ImageUrl = "123"},
                new TestForUI(){CategoryName = "Hjemmebagt Rugbrød", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Sandwich", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Salater", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Søde sager", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Varme Drikke", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Friskpresset Juice", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Smoothies", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Matcha", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Drikke", ImageUrl = "123"},
                new TestForUI(){CategoryName = "Hjemmebagt Rugbrød", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Sandwich", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Salater", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Søde sager", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Varme Drikke", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Friskpresset Juice", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Smoothies", ImageUrl = "456"},
                new TestForUI(){CategoryName = "Matcha", ImageUrl = "456"}
            };
            return testforui;
        }

        public List<TestForUI> GetTestForUIMenu()
        {
            List<TestForUI> testforui = new List<TestForUI>()
            {
                new TestForUI(){CategoryName = "Coca-Cola", ImageUrl = "kr. 28,00"},
                new TestForUI(){CategoryName = "Coca-Cola Zero", ImageUrl = "kr. 28,00"},
                new TestForUI(){CategoryName = "Danskvand", ImageUrl = "kr. 28,00"},
                new TestForUI(){CategoryName = "Kildevand", ImageUrl = "kr. 18,00"},
                new TestForUI(){CategoryName = "Milkshake", ImageUrl = "kr. 50,00"},
                new TestForUI(){CategoryName = "Iskaffe", ImageUrl = "kr. 48,00"}
            };
            return testforui;

        }

        
    }
}
