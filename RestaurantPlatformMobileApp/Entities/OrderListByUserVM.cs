﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RestaurantPlatformMobileApp.Entities
{
    public class OrderListByUserVM
    {
        public int OrderID { get; set; }
        public int UserID { get; set; }
        /// <summary>
        /// 0: Pending, 1: Complete
        /// </summary>
        public int Status { get; set; }
        public DateTime CreateDT { get; set; }

        public int RestaurantID { get; set; }
        public string RestaurantName { get; set; }

        public List<OrderItem> OrderItem { get; set; }

        public string OrderCount
        {
            get
            {
                if (OrderItem == null) 
                {
                    return string.Empty;
                }
                else
                {
                    if (OrderItem.Count == 0)
                    {
                        return string.Empty;
                    }
                    else
                    {
                        return OrderItem.Sum(ci => ci.Quantity).ToString();
                    }


                }
            }
        }

        public string TotalOfOrder
        {
            get
            {
                float totalPrice = 0;
                if (OrderItem != null) //this.Cart.Count <= 0
                {
                    foreach (OrderItem item in OrderItem)
                    {
                        totalPrice += item.Price * item.Quantity;
                    }
                }
                return totalPrice.ToString("C2");
            }
        }

        public string CreateDTWithFormatDK
        {
            get
            {
                return CreateDT.ToString("MM/dd/yyyy HH:mm");                
            }
        }
    }
}
