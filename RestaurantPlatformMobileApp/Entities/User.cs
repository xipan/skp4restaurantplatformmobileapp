﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantPlatformMobileApp.Entities
{
    public class User
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string AfterName { get; set; }
        public string Email { get; set; }
        public string PassWord { get; set; }
        /// <summary>
        /// UserType 0: employee, 1: customer
        /// </summary>
        public int UserType { get; set; }
        public int RestaurantID { get; set; }
    }
}
