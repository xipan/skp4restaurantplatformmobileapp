﻿using System.Collections.Generic;
using System.ComponentModel;
using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using Plugin.Badge.Droid;
using Xamarin.Forms.Platform.Android.AppCompat;

namespace RestaurantPlatformMobileApp.Droid.PageRenderer
{
    public class StyledTabbedPage : TabbedPage
    {
    }
    public class StyledTabbedPageRenderer : BadgedTabbedPageRenderer
    {
        public StyledTabbedPageRenderer(Context context) : base(context) { }


        private TabLayout tabLayout = null;

        protected override void OnElementChanged(ElementChangedEventArgs<TabbedPage> e)
        {
            base.OnElementChanged(e);

            this.tabLayout = (TabLayout)this.GetChildAt(1);

            //Method 2  
            changeTabsFont();

            //Method 1  
            tabLayout.TabMode = TabLayout.ModeScrollable;
            tabLayout.TabGravity = TabLayout.GravityFill;            
        }

        private void changeTabsFont()
        {
            //Typeface font = Typeface.CreateFromAsset(Android.App.Application.Context.Assets, "fonts/" + Constants.FontStyle);  
            ViewGroup vg = (ViewGroup)tabLayout.GetChildAt(0);
            int tabsCount = vg.ChildCount;
            string title;
            for (int j = 0; j < tabsCount; j++)
            {
                ViewGroup vgTab = (ViewGroup)vg.GetChildAt(j);
                int tabChildsCount = vgTab.ChildCount;
                for (int i = 0; i < tabChildsCount; i++)
                {
                    Android.Views.View tabViewChild = vgTab.GetChildAt(i);                    
                    if (tabViewChild is TextView)
                    {
                        //((TextView)tabViewChild).Typeface = font;                          
                        //((TextView)tabViewChild).TextSize = 3;
                        title = ((TextView)tabViewChild).Text;
                        //((TextView)tabViewChild).Text = title.Substring(0, 1).ToUpper() + title.Substring(1).ToLower();
                        //((TextView)tabViewChild).SetCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        //((TextView)tabViewChild).SetPadding
                        //((TextView)tabViewChild).SetPaddingRelative(0, -30, 0, 0);
                        //((TextView)tabViewChild).TextAlignment = Android.Views.TextAlignment.ViewEnd;
                    }
                }
            }
        }
    }
}